code golf # 2
================

Description
-----------

The strings inside of our two hashes have somehow become tainted with tildes(~), backticks(`), and carets(^).  We don't want them there.

These strings are nested within multiple hashes and arrays.  

Add logic to the Hash#sanitize_strings! method(located in **hash.rb**) that will remove the unwanted characters from the strings inside a hash without removing any other characters.  The two hashes are structured differently, so your method must work for any hash.

Instructions
------------

Run ```bundle install``` and then run ```rspec hash_spec.rb``` to test your code.  

Feel free to examine the test to see what is expected.

Green text and ```0 failures``` means that you have passed the test and your code works.

When you're done, push your code up to a new branch.