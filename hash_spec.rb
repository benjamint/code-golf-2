require './hash'

describe Hash, "#sanitize_strings!" do
  before :each do
    @hash1 = {
                users: [
                  {
                    id: 1,
                    username: "george",
                    posts: [
                      {
                        title: "Some post",
                        body: "Lorem Ipsum is ^simply dummy text of the printing and typesetting industry. ",
                        comments: [
                          {user_id: 3, body: "Whatever."}
                        ]
                      },
                      {
                        title: "Another post",
                        body: "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown ~printer took a `galley of type and scrambled it to make a type specimen book.",
                        comments: [
                          {user_id: 3, body: "georg~e su~cks~"},
                          {user_id: 1, body: "shut u^p"}
                        ]
                      },
                      {
                        title: "Yet another post",
                        body: "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
                        comments: []
                      }    
                    ]
                  },
                  {
                    id: 1,
                    username: "bob*",
                    posts: [
                      {
                        title: "My post",
                        body: "It was popularised in the 1960s with`` the release of Letraset sheets~ containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`",
                        comments: [
                          {user_id: 1, body: "Foo."},
                          {user_id: 3, body: "Bar."},
                          {user_id: 1, body: "B^az."}
                        ]
                      },
                      {
                        title: "More importan`t information",
                        body: "Contrary to popular belief, Lorem Ipsum is not simply random text.",
                        comments: [
                          {user_id: 1, body: "you'`re so^ random~"}
                        ]
                      }
                    ]
                  }
                ]
              }

    @hash2 = {
      first_value: [
        {
          hey: [
            {there: "m^an"},
            {
              whats: [
                {going: "on`?~"}
              ] 
            }
          ]
        }
      ] 
    }


    @result1 = {
                users: [
                  {
                    id: 1,
                    username: "george",
                    posts: [
                      {
                        title: "Some post",
                        body: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ",
                        comments: [
                          {user_id: 3, body: "Whatever."}
                        ]
                      },
                      {
                        title: "Another post",
                        body: "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                        comments: [
                          {user_id: 3, body: "george sucks"},
                          {user_id: 1, body: "shut up"}
                        ]
                      },
                      {
                        title: "Yet another post",
                        body: "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
                        comments: []
                      }    
                    ]
                  },
                  {
                    id: 1,
                    username: "bob*",
                    posts: [
                      {
                        title: "My post",
                        body: "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                        comments: [
                          {user_id: 1, body: "Foo."},
                          {user_id: 3, body: "Bar."},
                          {user_id: 1, body: "Baz."}
                        ]
                      },
                      {
                        title: "More important information",
                        body: "Contrary to popular belief, Lorem Ipsum is not simply random text.",
                        comments: [
                          {user_id: 1, body: "you're so random"}
                        ]
                      }
                    ]
                  }
                ]
              }

    @result2 = {
      first_value: [
        {
          hey: [
            {there: "man"},
            {
              whats: [
                {going: "on?"}
              ] 
            }
          ]
        }
      ] 
    }

  end

  it "scrubs every string in a hash" do
    @hash1.sanitize_strings!
    expect(@hash1).to eq(@result1)
    @hash2.sanitize_strings!
    expect(@hash2).to eq(@result2)
  end

end